/**
 * Created by quentin on 28/03/2016.
 */
angularModule.controller('TrackviewCtrl', function($scope, HistoriqueGPS) {

  $scope.addGpsWaypoints = function(a, b) {
    HistoriqueGPS.addPointsGPS(a, b);
    console.log(HistoriqueGPS.getPointsGPS());
    init();
  };

  var runningCoords = HistoriqueGPS.getPointsGPS();
  var gmap = document.getElementById('map');
  var map;
  var settings = {
    home: {
      latitude: runningCoords[0].lat, //46.138683,
      longitude: runningCoords[0].lng //-1.174177
    },
    icon_url: '',
    zoom: 16
  };

  var coords = new google.maps.LatLng(settings.home.latitude, settings.home.longitude);

  var options = {
    zoom: settings.zoom,
    scrollwheel: false,
    center: coords,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    mapTypeControl: true,
    scaleControl: true,
    streetViewControl: false,
    zoomControlOptions: {
      style: google.maps.ZoomControlStyle.DEFAULT
    },
    overviewMapControl: true
  };

  map = new google.maps.Map(gmap, options);

  var end = new google.maps.Marker({
    position: new google.maps.LatLng(runningCoords[0].lat, runningCoords[0].lng),
    map: map,
    icon: {
      url: 'http://www.supah.it/dribbble/020/end.png?v=3',
      origin: new google.maps.Point(0, 0)
    },
    draggable: false
  });

  var start = new google.maps.Marker({
    position: new google.maps.LatLng(HistoriqueGPS.dernierPointGPS().lat, HistoriqueGPS.dernierPointGPS().lng),
    map: map,
    icon: {
      url: 'http://www.supah.it/dribbble/020/start.png?v=3',
      origin: new google.maps.Point(0, 0)
    },
    draggable: false
  });

  var info = new google.maps.InfoWindow({
    content: settings.text
  });

  /* No Animation */
  /*
   var runPath = new google.maps.Polyline({
   path: runningCoords,
   geodesic: true,
   strokeColor: '#58a7e2',
   strokeOpacity: 1.0,
   strokeWeight: 3
   });
   runPath.setMap(map);
   */

  /* Animation */
  var i = 0;

  function animPath() {
    if (i > runningCoords.length) {
      return false;
    }

    try {
      var dept_lat = runningCoords[i].lat;
      var dept_lng = runningCoords[i].lng;
      //console.log("Running 1", runningCoords[i]);
    }
    catch (ex) {
      dept_lat = undefined;
      dept_lng = undefined;
    }

    try {
      var arr_lat = runningCoords[i + 1].lat;
      var arr_lng = runningCoords[i + 1].lng;
      //console.log("Running 1", runningCoords[i]);
    }
    catch (ex) {
      arr_lat = undefined;
      arr_lng = undefined;
    }

    var departure = new google.maps.LatLng(dept_lat, dept_lng); //Set to whatever lat/lng you need for your departure location
    var arrival = new google.maps.LatLng(arr_lat, arr_lng); //Set to whatever lat/lng you need for your arrival location
    var line = new google.maps.Polyline({
      path: [departure, departure],
      strokeColor: "#58a7e2",
      strokeOpacity: 1,
      strokeWeight: 3,
      geodesic: true, //set to false if you want straight line instead of arc
      map: map
    });
    var step = 0;
    var numSteps = 25; //Change this to set animation resolution
    var timePerStep = 5; //Change this to alter animation speed
    var interval = setInterval(function() {
      step += 1;
      if (step > numSteps) {
        clearInterval(interval);
        i++;
        animPath();
      }
      else {
        var are_we_there_yet = google.maps.geometry.spherical.interpolate(departure, arrival, step / numSteps);
        line.setPath([departure, are_we_there_yet]);
      }
    }, timePerStep);
  }

  google.maps.event.addListenerOnce(map, 'idle', function() {
    setTimeout(init, 300);
  });

  function init() {
    animPath();
  }

  $.ajax({
    type: "GET",
    url: "../data/nmealog.gpx",
    dataType: "xml",
    success: function(xml) {
      var points = [];
      var bounds = new google.maps.LatLngBounds();
      $(xml).find("trkpt").each(function () {
        var lat = $(this).attr("lat");
        var lon = $(this).attr("lon");
        console.log("LAT = " + lat + " LONG = " + lon);

        var p = new google.maps.LatLng(lat, lon);
        points.push(p);
        bounds.extend(p);
        HistoriqueGPS.addPointsGPS(lat, lon);

        console.log("DERNIER = ", HistoriqueGPS.dernierPointGPS());
        console.log("GET POINTS GPS", HistoriqueGPS.getPointsGPS()[5]);
      });


      var poly = new google.maps.Polyline({
        // use your own style here
        path: points,
        strokeColor: "#f00",
        strokeOpacity: .7,
        strokeWeight: 4
      });

      poly.setMap(map);

      // fit bounds to track
      map.fitBounds(bounds);
    }
  });

});
