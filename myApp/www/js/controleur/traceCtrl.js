/**
 * Created by quentin on 28/03/2016.
 */

  //BSpline
angularModule.controller('TracerCtrl', function($scope, HistoriqueGPS) {
  $scope.afficherCarte=true;

  console.log(HistoriqueGPS.dernierPointGPS());

  console.log(" Hi");
  var sm = new ScribbleMap(document.getElementById('ScribbleMap'));

  var tmp = {
    "meta": {
      "title": "",
      "description": "",
      "createdAt": "http://www.scribblemaps.com",
      "id": null,
      "listType": "public"
    },
    "view": {
      "zoom": 13,
      "mapType": "hybrid",
      "clustering": "none",
      "center": HistoriqueGPS.dernierPointGPS()
    },
    "styles": [{
      "id": "dcd4e2dd72",
      "color_1": {
        "color": "#ff0000",
        "opacity": 0.65
      },
      "color_2": {
        "color": "#00ff00",
        "opacity": 0.65
      },
      "weight": 5,
      "lineType": 0,
      "segment": [5, 5, 5],
      "cap": 0,
      "join": 0,
      "size": {
        "x": 5,
        "y": 5
      }
    }, {
      "id": "86c441099d",
      "color_1": {
        "color": "#ff0000",
        "opacity": 0.65
      },
      "weight": 5,
      "lineType": 0,
      "segment": [5, 5, 5],
      "cap": 0,
      "join": 0
    }],
    "overlays": [{
      "id": "92928e27e6",
      "visible": true,
      "editable": true,
      "points": [HistoriqueGPS.dernierPointGPS()],
      "styleId": "dcd4e2dd72",
      "type": 19
    }, {
      "id": "4fd6c82708",
      "visible": true,
      "editable": true,
      "points": [HistoriqueGPS.dernierPointGPS()],
      "styleId": "86c441099d",
      "type": 1
    }]
  };

  sm.map.loadSmJSON(tmp);

  sm.ui.setMapTypes();
  sm.ui.setAvailableTools(["drag", "scribble"]);
  sm.ui.styleControl(scribblemaps.ControlType.SEARCH, {
    "top": "5000px",
    "left": "50px",
    "width": "50px"
  });
  sm.ui.styleControl(scribblemaps.ControlType.LINE_SETTINGS, {
    "top": "5000px",
    "left": "50px",
    "width": "50px"
  });
  sm.ui.styleControl(scribblemaps.ControlType.LINE_COLOR, {
    "top": "5000px",
    "left": "50px",
    "width": "50px"
  });
  sm.ui.styleControl(scribblemaps.ControlType.FILL_COLOR, {
    "top": "5000px",
    "left": "50px",
    "width": "50px"
  });

  sm.view.setCenter(HistoriqueGPS.dernierPointGPS()); //LR = [46.13823057699896,-1.1775922166825303]);
  console.log("point");
  sm.draw.point(HistoriqueGPS.dernierPointGPS(), 5);
  sm.view.setZoom(13);
  var result = [];

  /*
   sm.ui.setMapTypes([
   {
   label:"Road",
   id:"Road"
   }]);
   sm.map.setType("ROAD");
   */

  window.onload = function() {
    console.log("onload");
  };

  $scope.test = function() {
    try {

      // console.log("Before tmp");
      // console.log(JSON.stringify(sm.data.getSmJSON().overlays[1]));

      //tmp.overlays[0].points=JSON.stringify(sm.data.getSmJSON().overlays[1].points);
      //tmp.overlays[1].points=JSON.stringify(sm.data.getSmJSON().overlays[1].points);

      //console.log("After tmp");
      //console.log(tmp.overlays[1]);

      //sm.map.loadSmJSON(JSON.stringify(tmp));
      //console.log("after load");
      //console.log(JSON.stringify(tmp));

      result = JSON.stringify(sm.data.getSmJSON().overlays[1].points);
      console.log("JSON");
      $scope.jsonToSend=result;
      if(sm.data.getSmJSON().overlays[1].points.length>1) {
        $scope.afficherCarte = false;
        console.log(result);
      }
      //var div = document.createElement("div");
      //div.style.width = "250px";
      //div.style.height = "50px";
      //div.innerHTML = "Dessin OK";
      //sm.map.getOverlays().disableEdit();

      //sm.ui.showCustomPanel(div);
    }
    catch (ex) {}

  };
});
