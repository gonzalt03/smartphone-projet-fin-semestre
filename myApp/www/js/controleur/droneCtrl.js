/**
 * Created by quentin on 28/03/2016.
 */
angularModule.controller('DronevirtuelCtrl', function($scope, HistoriqueGPS, ClockSrv) {

    //Initialisation
    coordLatDrone = HistoriqueGPS.dernierPointGPS().lat;
    coordLngDrone = HistoriqueGPS.dernierPointGPS().lng;
    regulation = 0;
    $scope.bloquerDrone = false;
    vitesseBase = 0.0000010;
    appareilGyroX = 0;
    appareilGyroY = 0;

    //Initialisation de Google Maps
    //Image par défaut du drone pour le google maps marker
    var imageMarqueur = {
      url: "img/drone.png"
    };
    //Création de la Maps
    var myLatLng = new google.maps.LatLng(50, 50),
      myOptions = {
        zoom: 23,
        center: myLatLng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      },
      map = new google.maps.Map(document.getElementById('map_drone'), myOptions),
      marker = new google.maps.Marker({
        icon: imageMarqueur,
        position: myLatLng,
        map: map
      });
    //Ajout du marker à la map
    marker.setMap(map);


    //Mise à Jour de la carte avec le pointeur
    marker.setPosition(new google.maps.LatLng(46.1473735, -1.1123617));
    map.panTo(new google.maps.LatLng(46.1473735, -1.1123617));


    //Si retour HOME
    $scope.backHome = function () {
      coordLatDrone = HistoriqueGPS.dernierPointGPS().lat;
      coordLngDrone = HistoriqueGPS.dernierPointGPS().lng;
      marker.setPosition(new google.maps.LatLng(coordLatDrone, coordLngDrone));
      map.panTo(new google.maps.LatLng(coordLatDrone, coordLngDrone));
    };

    //Si retour URGENCE
    $scope.urgence = function () {
      $scope.bloquerDrone = true;
    };

    //Si débloquage
    $scope.debloquer = function () {
      $scope.bloquerDrone = false;
    };

    //Ball sur l'écran
    var ball = document.querySelector('.ball');
    var garden = document.querySelector('.garden');
    var output = document.querySelector('.output');

    var maxX = garden.clientWidth - ball.clientWidth;
    var maxY = garden.clientHeight - ball.clientHeight;

  $scope.$on('$ionicView.afterEnter', function() {
    //BOUCLE CHANGEMENT
    ClockSrv.startClock(function () {

      var y = appareilGyroY; // In degree in the range [-180,180]
      var x = appareilGyroX; // In degree in the range [-90,90]

      // Puissance avancement
      if(x>50)
        $scope.vitesseAvancement=5;
      else if (x<-50)
        $scope.vitesseAvancement=-5;
      else if (x < 0){
        $scope.vitesseAvancement=Math.ceil((x)/10);
      }else
        $scope.vitesseAvancement=Math.floor((x)/10);
      console.log($scope.vitesseAvancement);

      // angleRotation
      if(y>50)
        $scope.angleRotation=-5;
      else if (y<-50)
        $scope.angleRotation=5;
      else if (y < 0) {
        $scope.angleRotation=Math.floor((-y)/10);
      } else
        $scope.angleRotation=Math.ceil((-y)/10);
      console.log($scope.angleRotation);

      //Afficher information technique de déplacement
      /*
       output.innerHTML = "Y : " + Math.round(x).toFixed(2) + "\n";
       output.innerHTML += "X : " + Math.round(y).toFixed(2) + "\n";

       output.innerHTML += "Avancement Y : " + $scope.vitesseAvancement + "\n";
       output.innerHTML += "Avancement X : " + $scope.angleRotation + "\n";
       */

      // Because we don't want to have the device upside down
      // We constrain the x value to the range [-90,90]
      if (x > 90) {
        x = 90
      }
      ;
      if (x < -90) {
        x = -90
      }
      ;

      // To make computation easier we shift the range of
      // x and y to [0,180]
      x += 90;
      y += 90;

      // 10 is half the size of the ball
      // It center the positioning point to the center of the ball
      ball.style.top = (maxX * x / 180 - 10) + "px";
      ball.style.left = (maxY * y / 180 - 10) + "px";

      //Mise à jour du Marker sur la carte Google Maps
      if (!$scope.bloquerDrone) {
        regulation = 0;
        coordLatDrone = coordLatDrone + (vitesseBase * $scope.vitesseAvancement);
        coordLngDrone = coordLngDrone + (vitesseBase * $scope.angleRotation);
        console.log("Lat : " + coordLatDrone + "  -  Long : " + coordLngDrone);
        marker.setPosition(new google.maps.LatLng(coordLatDrone, coordLngDrone));
        map.panTo(new google.maps.LatLng(coordLatDrone, coordLngDrone));
      }

    });


    window.addEventListener('deviceorientation', function handleOrientation(event) {
      appareilGyroX = -event.gamma;
      appareilGyroY = event.beta;
    });

  });

  $scope.$on('$ionicView.afterLeave', function(){
    ClockSrv.stopClock();
    //alert("Clock stop ok");
  });

  });

