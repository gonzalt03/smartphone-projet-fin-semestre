// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic','ngIOS9UIWebViewPatch','starter.controllers', 'starter.services'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

     //$cordovaPlugin.someFunction().then(success, error);

 /*   var mapOptions = {
      zoom: 8,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("map"),
        mapOptions);
    loadGPXFileIntoGoogleMap(map, "../data/nmealog.gpx");
    */

  });
})

/*$(document).ready(function() {
    var mapOptions = {
      zoom: 8,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("map"),
        mapOptions);
    loadGPXFileIntoGoogleMap(map, "../data/nmealog.gpx");
});
*/

  .config(function($ionicConfigProvider) { $ionicConfigProvider.tabs.position('bottom'); })

  .factory('ClockSrv', function($interval){
    var clock = null;
    var service = {
      startClock: function(fn){
        if(clock === null){
          clock = $interval(fn, 1);
        }
      },
      stopClock: function(){
        if(clock !== null){
          $interval.cancel(clock);
          clock = null;
        }
      }
    };
    return service;
  })

.factory('HistoriqueGPS', function() {
	var pointsGPS =  [{
    lat: 46.138683,
    lng: -1.174177
  }, {
    lat: 46.138549,
    lng: -1.171184
  }, {
    lat: 46.139568,
    lng: -1.172203
  }, {
    lat: 46.138713,
    lng: -1.174134
  }, {
    lat: 46.139620,
    lng: -1.170489
  }];

	return {
		getPointsGPS: function(){
			return pointsGPS
			},
		addPointsGPS: function(lat,long){
			pointsGPS.push({"lat":lat,"lng":long});
			},
		dernierPointGPS: function(){
			return pointsGPS[pointsGPS.length - 1];
			}
		}

})



.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the tabs directive
    .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html'
  })

  // Each tab has its own nav history stack:

  .state('tab.tracer', {
    url: '/tracer',
    views: {
      'tab-tracer': {
        templateUrl: 'templates/tab-tracer.html',
        controller: 'TracerCtrl'
      }
    }
  })

  .state('tab.dronevirtuel', {
    url: '/dronevirtuel',
    views: {
      'tab-dronevirtuel': {
        templateUrl: 'templates/tab-dronevirtuel.html',
        controller: 'DronevirtuelCtrl'
      }
    }
  })

  .state('tab.trackview', {
      url: '/trackview',
      views: {
        'tab-trackview': {
          templateUrl: 'templates/tab-trackview.html',
          controller: 'TrackviewCtrl'
        }
      }
    });


  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tab/tracer');

});
